# Rustic Zen:
Hardware Accelerated 2D Ray Tracing Framework & Rust Impementation of 
[Zenphoton](https://scanlime.org/2013/05/high-quality-zen/).

![Crates.io Version](https://img.shields.io/crates/v/rustic-zen)
![docs.rs](https://img.shields.io/docsrs/rustic-zen)
![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/IGBC%2Frustic-zen?branch=master)
![Crates.io Downloads (recent)](https://img.shields.io/crates/dr/rustic-zen)


This raytracer simulates individual photons bouncing around a 2D sandbox, then 
plots the path it took. The image is comprised of the sum of millions of individual
photon plots, creating a unique woven-like texture in the resulting images.

Rustic Zen renders artworks from a scene definition by simulating individual
photons and tracing their path as they bounce through a 2D. space.

Photons are generated by Lights and interact with Objects. Each interaction
results in either the photon being absorbed, or being allowed to continue with
a new direction, based on the rules defined by the Object's Material.

This implementation uses Vulkan to provide hardware accelerated rendering of the 
images (although the ray tracing calculations are still done on the CPU)

## What this library Provides:
This library holds only the rendering framework and models for defining a scene.
Functionality for defining a scene, i.e. generative and animation algorithms are
not provided here. The focus of Rustic-Zen is providing the raytrace algorithms
for rendering a static scene. 

Rustic-Zen provides a single basic shader, for backwards compatiblity with prior
art. It is expected that dedicated library users will use the exposed Material
trait to create your own shaders.

## Example usage:
``` rust
extern crate rustic_zen;
extern crate png;

use rustic_zen::prelude::*;
use rustic_zen::material::hqz_legacy;
// To use encoder.set()
use png::HasParameters;

use std::sync::Arc;

fn main() {
    // Set up constants.
    let width: f64 = 1920.0;
    let height: f64 = 1080.0;
    
    // Build a basic Material
    let m = hqz_legacy(0.3, 0.3, 0.3);
    
    // Build a basic Object
    let o = Object::line_from_points((0.0,(height * 0.75)), (width, (height * 0.75)), m);

    // Build a basic Light
    let l = Light{
        power: 1.0.into(),
        x: (width / 2.0).into(),
        y: (height / 2.0).into(),
        polar_angle: 0.0.into(),
        polar_distance: 0.0.into(),
        ray_angle: (360.0, 0.0).into(), // creates a linear range
        wavelength: Sampler::new_blackbody(4500.0),
    };
    // Construct a renderer object and add the light and object to it.
    let s = Scene::new(width as usize, height as usize).with_object(o).with_light(l);
    
    // Create an image to render into
    let mut image = Arc::new(Image::new(width as usize, height as usize));

    // Render Image
    println!("Tracing Rays");
    let (rays, image) = s.render(RenderConstraint::TimeMS(1000), 1, &mut image);
    // this call should probably be more like 5000 - 15000 ms and use your number of threads,
    // but this example is run by various CI tools, and tests can't take too long.
    // Output the Image as a Vec<u8>
    println!("Serializing!");
    let data = image.to_rgba8(rays, 0.7, 1.2);
    
    let mut encoder = png::Encoder::new(w, width as u32, height as u32);
    encoder.set(png::ColorType::RGBA).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();
    writer.write_image_data(&data).unwrap();
}
```

## Example Output:
![example output](https://gitlab.com/IGBC/rustic-zen/-/jobs/6386009123/artifacts/raw/image/prism.jpg)

## Building:
This library by default uses Vulkano for access to hardware acceleration. To use this you will need a functioning 
Vulkan development setup, as well as the build dependencies for Vulkano for your platform. See the section 
"Setup and Troubleshooting" of the [Vulkano 0.34 Readme](https://crates.io/crates/vulkano/0.34.1) for more information
on configuring your machine.

If you are unable to provide a Vulkan environment (for instance if building on a CI server or older hardware) the Vulkano
dependancies can be disabled by disabling default features. This will leave only software rendering available. More information
[here](https://doc.rust-lang.org/cargo/reference/features.html#dependency-features)

## Versioning:
In beta version numbers will be `0.*.*` every minor release will be breaking
Post `1.0.0` semantic versioning will be used

## Licence
From Version 0.2.1 The library code, all tests, and documentation is licenced under the Mozilla Public License, v. 2.0.
See the the LICENCE file for the full terms of this licence.

The code in the examples directory, and all artworks created by the examples are copyright (SEGFAULT), and are licenced under
the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License, except where specified otherwise.
See the LICENCE-EXAMPLES file for the full terms of this licence.
